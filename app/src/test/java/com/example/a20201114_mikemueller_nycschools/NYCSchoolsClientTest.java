package com.example.a20201114_mikemueller_nycschools;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a20201114_mikemueller_nycschools.client.NYCSchoolsClient;
import com.example.a20201114_mikemueller_nycschools.client.NYCSchoolsService;
import com.example.a20201114_mikemueller_nycschools.domain.School;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NYCSchoolsClientTest {

    @Mock
    Call<List<School>> mockedCall;
    @Mock
    RecyclerView recyclerView;
    @Mock
    FragmentActivity activity;
    @Mock
    SchoolAdapterFactory factory;
    @Mock
    SchoolAdapter adapter;
    @Mock
    NYCSchoolsService schoolsService;
    @Mock
    FirstFragment firstFragment;

    @InjectMocks
    NYCSchoolsClient subject;

    @Test
    public void testApiResponse() {

    List<School> expectedSchools = Collections.singletonList(new School());

    when(schoolsService.fetchSchools()).thenReturn(mockedCall);
    when(factory.create(activity, expectedSchools, firstFragment)).thenReturn(adapter);
    Mockito.doAnswer(invocation -> {
        Callback<List<School>> callback = invocation.getArgument(0);
        callback.onResponse(mockedCall, Response.success(expectedSchools));
        return null;
    }).when(mockedCall).enqueue(any(Callback.class));

    subject.fetchSchools(recyclerView, activity, factory, firstFragment);

    verify(recyclerView).setAdapter(adapter);
    verify(adapter).setOnClick(subject);
    }
}