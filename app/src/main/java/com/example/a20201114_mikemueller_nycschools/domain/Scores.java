package com.example.a20201114_mikemueller_nycschools.domain;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class Scores {

    @SerializedName("dbn")
    String dbn;

    @SerializedName("school_name")
    String schoolName;

    @SerializedName("sat_critical_reading_avg_score")
    String readingScore;

    @SerializedName("sat_math_avg_score")
    String mathScore;

    @SerializedName("sat_writing_avg_score")
    String writingScore;
}
