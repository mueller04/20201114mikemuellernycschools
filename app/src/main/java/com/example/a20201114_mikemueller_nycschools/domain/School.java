package com.example.a20201114_mikemueller_nycschools.domain;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class School {
    @SerializedName("school_name")
    String schoolName;

    @SerializedName("dbn")
    String dbn;

    @SerializedName("phone_number")
    String phoneNumber;
}
