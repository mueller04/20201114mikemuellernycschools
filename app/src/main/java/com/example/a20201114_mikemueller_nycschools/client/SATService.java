package com.example.a20201114_mikemueller_nycschools.client;

import com.example.a20201114_mikemueller_nycschools.domain.Scores;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface SATService {

    @GET("resource/f9bf-2cp4.json")
    Call<List<Scores>> fetchScores();
}
