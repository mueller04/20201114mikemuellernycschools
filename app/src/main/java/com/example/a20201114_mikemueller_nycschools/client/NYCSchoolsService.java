package com.example.a20201114_mikemueller_nycschools.client;

import com.example.a20201114_mikemueller_nycschools.domain.School;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface NYCSchoolsService {

    @GET("resource/s3k6-pzi2.json")
    Call<List<School>> fetchSchools();
}
