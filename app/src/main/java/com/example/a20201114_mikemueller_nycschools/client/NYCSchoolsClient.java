package com.example.a20201114_mikemueller_nycschools.client;

import com.example.a20201114_mikemueller_nycschools.domain.School;
import com.example.a20201114_mikemueller_nycschools.view.FirstFragmentPresenter;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/** I would not make this implement OnItemClicked, I would have onResponse set the school data back
in another view object which would take care of the adapter and the onItemClicked implementation**/
public class NYCSchoolsClient {

    private NYCSchoolsService service;

    public NYCSchoolsClient(NYCSchoolsService service) {
        this.service = service;
    }

    public void fetchSchools(FirstFragmentPresenter presenter) {
        Call<List<School>> call = service.fetchSchools();

        call.enqueue(new Callback<List<School>>() {
            @Override
            public void onResponse(Call<List<School>> call, Response<List<School>> response) {
                presenter.setupSchoolListView(response.body());
            }

            @Override
            public void onFailure(Call<List<School>> call, Throwable t) {
                //would add a toast with a failure message
            }
        });
    }
}
