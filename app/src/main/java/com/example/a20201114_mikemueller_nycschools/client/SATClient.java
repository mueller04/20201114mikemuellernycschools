package com.example.a20201114_mikemueller_nycschools.client;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.a20201114_mikemueller_nycschools.domain.School;
import com.example.a20201114_mikemueller_nycschools.domain.Scores;
import com.example.a20201114_mikemueller_nycschools.view.FirstFragmentPresenter;

import java.util.List;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SATClient {

    private SATService service;

    public SATClient(SATService service) {
        this.service = service;
    }

    public void fetchSATScores(School school, FirstFragmentPresenter presenter) {

        Call<List<Scores>> call = service.fetchScores();

        call.enqueue(new Callback<List<Scores>>() {

            //shortcut, would use code available in min sdk
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<List<Scores>> call, Response<List<Scores>> response) {
                List<Scores> schools = response.body()
                        .stream()
                        .filter(score -> score.getDbn().equals(school.getDbn())).collect(Collectors.toList());
                if (!schools.isEmpty()) {
                    Scores scores = schools.get(0);
                    presenter.navigateToSATScores(scores, school);
                } else {
                    presenter.noDataFoundToast();
                }
            }

            @Override
            public void onFailure(Call<List<Scores>> call, Throwable t) {
                //would add a toast with a failure message
            }
        });
    }
}
