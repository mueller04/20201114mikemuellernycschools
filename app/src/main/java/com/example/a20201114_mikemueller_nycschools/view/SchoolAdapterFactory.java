package com.example.a20201114_mikemueller_nycschools.view;

import androidx.fragment.app.FragmentActivity;

import com.example.a20201114_mikemueller_nycschools.domain.School;

import java.util.List;

public class SchoolAdapterFactory {

    public SchoolAdapter create(FragmentActivity activity, List<School> schools) {
        return new SchoolAdapter(activity, schools);
    }
}
