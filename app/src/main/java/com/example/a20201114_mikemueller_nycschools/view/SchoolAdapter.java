package com.example.a20201114_mikemueller_nycschools.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a20201114_mikemueller_nycschools.R;
import com.example.a20201114_mikemueller_nycschools.domain.School;

import java.util.List;

public class SchoolAdapter extends RecyclerView.Adapter<SchoolAdapter.SchoolViewHolder> {

    List<School> data1;
    Context context;

    public SchoolAdapter(Context ct, List<School> s1) {
        context = ct;
        data1 = s1;
    }

    private OnItemClicked onClick;

    public interface OnItemClicked {
        void onItemClick(School position);
    }

    @NonNull
    @Override
    public SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.my_row, parent, false);
        return new SchoolViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolViewHolder holder, int position) {
        School school = data1.get(position);
        holder.textView.setText(school.getSchoolName());

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.onItemClick(school);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data1.size();
    }

    public void setOnClick(OnItemClicked onClick)
    {
        this.onClick=onClick;
    }

    public class SchoolViewHolder extends RecyclerView.ViewHolder {

        TextView textView;

        public SchoolViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.textView);
        }
    }
}
