package com.example.a20201114_mikemueller_nycschools.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.a20201114_mikemueller_nycschools.R;

public class SecondFragment extends Fragment {

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
        TextView schoolNameView = view.findViewById(R.id.textview_school_name);
        schoolNameView.setText(args.getString("schoolName"));
        TextView mathView = view.findViewById(R.id.textview_math);
        mathView.setText(args.getString("readingScore"));
        TextView readingView = view.findViewById(R.id.textview_reading);
        readingView.setText(args.getString("mathScore"));
        TextView writingView = view.findViewById(R.id.textview_writing);
        writingView.setText(args.getString("writingScore"));
        TextView phoneView = view.findViewById(R.id.textview_phone_number);
        phoneView.setText(args.getString("phone"));

        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(SecondFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}