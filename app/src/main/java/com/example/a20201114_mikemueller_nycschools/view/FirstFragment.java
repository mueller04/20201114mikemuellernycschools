package com.example.a20201114_mikemueller_nycschools.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.a20201114_mikemueller_nycschools.R;

public class FirstFragment extends Fragment {

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //would use IoC
        FirstFragmentPresenter presenter = new FirstFragmentPresenter();
        presenter.getNYCSchools(view, getActivity(), this);
    }

    public void noDataFoundToast() {
        Toast toast=Toast.makeText(getContext(),"No data found for school", Toast.LENGTH_SHORT);
        toast.show();
    }
}