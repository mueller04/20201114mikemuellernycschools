package com.example.a20201114_mikemueller_nycschools.view;

import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.FragmentActivity;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a20201114_mikemueller_nycschools.R;
import com.example.a20201114_mikemueller_nycschools.client.NYCSchoolsClient;
import com.example.a20201114_mikemueller_nycschools.client.NYCSchoolsService;
import com.example.a20201114_mikemueller_nycschools.client.SATClient;
import com.example.a20201114_mikemueller_nycschools.client.SATService;
import com.example.a20201114_mikemueller_nycschools.domain.School;
import com.example.a20201114_mikemueller_nycschools.domain.Scores;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FirstFragmentPresenter implements SchoolAdapter.OnItemClicked {

    private FirstFragment firstFragment;
    private RecyclerView recyclerView;
    private FragmentActivity activity;

    public void getNYCSchools(View view, FragmentActivity activity, FirstFragment fragment) {
        this.firstFragment = fragment;
        this.activity = activity;

        //would design other layers to remove this logic from the view
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://data.cityofnewyork.us/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        NYCSchoolsService service = retrofit.create(NYCSchoolsService.class);

        //use IoC like Dagger
        NYCSchoolsClient client = new NYCSchoolsClient(service);
        recyclerView = view.findViewById(R.id.recycler_view);
        client.fetchSchools(this);
    }

    public void setupSchoolListView(List<School> schools) {
        //would implement dagger and handle via IoC
        SchoolAdapterFactory factory = new SchoolAdapterFactory();
        SchoolAdapter adapter = factory.create(activity, schools);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        adapter.setOnClick(this);
    }

    @Override
    public void onItemClick(School school) {
        // would move this logic elsewhere
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://data.cityofnewyork.us/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        SATService service = retrofit.create(SATService.class);

        SATClient client = new SATClient(service);
        client.fetchSATScores(school, this);
    }

    public void navigateToSATScores(Scores scores, School school) {
        // would use a parcelable or safe args
        Bundle bundle = new Bundle();
        bundle.putString("schoolName", scores.getSchoolName());
        bundle.putString("readingScore", scores.getReadingScore());
        bundle.putString("mathScore", scores.getMathScore());
        bundle.putString("writingScore", scores.getWritingScore());
        bundle.putString("phone", school.getPhoneNumber());
        NavHostFragment.findNavController(firstFragment)
                .navigate(R.id.action_FirstFragment_to_SecondFragment, bundle);
    }

    //would simplify dependencies probably using EventBus to talk back to view
    public void noDataFoundToast() {
        firstFragment.noDataFoundToast();
    }
}